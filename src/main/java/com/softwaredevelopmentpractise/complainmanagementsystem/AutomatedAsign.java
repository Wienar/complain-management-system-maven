/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

import java.util.ArrayList;

/**
 * this class handles the automated asignement of tickets. It decides with
 * different parameters which assignee is the best choice. we made some
 * assumptions in that process. TODO add more information about what assumptions
 * we made.
 *
 * @author admin
 */
public class AutomatedAsign {

	//DataCommands data = new DataCommands();
	//ArrayList<User> users = data.getUsers();
	//Ticket randomTestTicket = data.getRandomTicket(users);
	//ArrayList<Ticket> ticketList = data.getTickets(users);

	public Assignee asignTicket(String prio, Service service, ArrayList<User> users) {
		Assignee bestAssignee = null;
		int currentBestPoints = Integer.MIN_VALUE;
		for (User user : users) {
			if (user.getStatus().equals("assignee")) {
				Assignee assignee = (Assignee) user;
				int points = getPoints(assignee, service, prio);
				if (points > currentBestPoints) {
					bestAssignee = assignee;
					currentBestPoints = points;
				}
			}
		}
		//ArrayList<Ticket> openTickets = bestAssignee.getOpenTickets();
	
		//openTickets.add(ticket);
		//ticket.setAssignee(bestAssignee);
		//bestAssignee.setOpenTickets(openTickets);

		return bestAssignee;
	}

	public int getPoints(Assignee assignee, Service service, String prio) {
		int count = 0;
		//int openTickets = countTickets(assignee, ticketList);
		int openTickets = assignee.getOpenTickets().size();
		// if a assignee is strong in the required trait he gets extra points
		if (service.getRequiredTrait().equals(assignee.getMajorTrait())) {
			count = count + 60;
		} // if a assignee has the required trait, but isn't as strong in it, he gets less points
		else if (service.getRequiredTrait().equals(assignee.getMinorTrait())) {
			count = count + 50;
		}
		// a assignee gets dynamic points depending on how many open tickets he has
		if (prio.equals("low")) {
			count = count - (openTickets * 2);

		}
		//  the points drop faster when they have more open tickets.
		if (prio.equals("high")) {
			count = count - (openTickets * 3);
		}
		return count;
	}

	private int countTickets(Assignee assignee, ArrayList<Ticket> tickets) {
		int count = 0;
		for (Ticket ticket : tickets) {
			if (ticket.getAssignee().getUuid().equals(assignee.getUuid())) {
				count++;
			}
		}
		return count;
	}

}
