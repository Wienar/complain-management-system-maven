/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * This class will handle everything for the user once he logged in.
 *
 * @author admin
 */
public class UserEnvironment extends CommandsAbstract {

	private final String greetings = "Hello, \n you are in the User Screen "
		+ "\n you can file tickets and get the status of your existing tickets  ";
	private final String help = "\n list of all available commands: "
		+ "\n createTicket: initiates the ticket process"
		+ "\n logout: logs you out and you get back to the login screen";
	private final String error = "";

	Customer currentUser;

	public void start(Customer user) {
		currentUser = user;
		customer();
		standby();
	}

	/*private void standbyOLD() {
		String input = this.waitForInput();
		switch (input) {
			case "help":
				this.helpMessage(greetings, help);
				break;
			case "createTicket":
				//createTicket();
				break;
			case "logout": 
				break;
			default:
				this.inputNotSupported();
		}
		if (!input.equals("logout")) {
		standby();
		}
	}
	 */
	protected Scanner scanner = new Scanner(System.in);

	private void customer() {
		System.out.println("\nWelcome!!");
		System.out.println("\nCreate a new Ticket");
		System.out.println("View Tickets");
	}

	protected String waitForInput() {
		String input = scanner.nextLine();
		return input;

	}

	private void create() {

		System.out.println("Choose between Category");
		System.out.println("software");
		System.out.println("hardware");
		System.out.println("office");

		String category = scanner.next();
		Service service = null;

		if (!(category.equals("software") || category.equals("hardware") || category.equals("office"))) {
			System.out.println("abort mission, abort mission");
		} else {
			if (category.equals("software")) {
				service = new Service("random problem with everything hardware related", "hardware", "trait1");
			} else if (category.equals("hardware")) {
				service = new Service("random problem with everything software related", "software", "trait2");
			} else if (category.equals("office")) {
				service = new Service("random problem with office equiptment like a printer", "office", "trait3");
			}

			System.out.println("Please choose between high or low priority");
			String prio = scanner.next();
			if (!(prio.equals("high") || prio.equals("low"))) {
				System.out.println("abort abort, mission is a loss");
			} else {
				System.out.println("\nDescrption of the problem ");
				String body = scanner.next();

				System.out.println("\nSubmit the Ticket with yes or cancel with no ");
				String submit = scanner.next();

				if (submit.equals("yes")) {
					createTicket(prio, body, service);
				} else if (submit.equals("no")) {
					System.out.println("abort mission, abort mission");
				} else {
					System.out.println("you dun goofed, try again");
				}
			}

		}

	}

	private void createTicket(String prio, String body, Service service) {

		String uuid = UUID.randomUUID().toString();
		AutomatedAsign asign = new AutomatedAsign();
		DataCommands data = new DataCommands();

		Assignee ass = asign.asignTicket(prio, service, data.getUsers());

		long timestamp = Timestamp.valueOf(LocalDateTime.MIN).getTime();
		Date date = new Date(timestamp);
		Ticket ticket = new Ticket(uuid, prio, body, service, "open", ass, currentUser, date, null);

		System.out.println("\nThank you");

		System.out.println("Ticket submitted Succesfully.");

		System.out.println("\n Ticket Number:" + uuid);

		System.out.println(" \n Our Technician will get back to you soon");

		System.out.println("\n Email ID: support@srh.hochschule-heidelberg.de");

		System.out.println(" Telephone Number: 017623585");
	}

	private void viewTickets() {

		Random no = new Random();

		System.out.println("\n Tickets:" + "\n");
		for (int j = 0; j < 7; j++) {
			int showTicket = no.nextInt(67) * 78945;

			System.out.println(showTicket);

		}

	}

	private void standby() {
		String input = this.waitForInput();

		switch (input) {
			case "help":
				customer();
				this.message(help);
				break;
			case "create":
				create();
				break;
			case "view":
				viewTickets();
				break;

		}
		standby();
	}
}
