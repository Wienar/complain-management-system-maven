/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class TestCases {

	DataCommands data = new DataCommands();
	AutomatedAsign auto = new AutomatedAsign();
	ArrayList<User> users = data.getUsers();
	ArrayList<Ticket> tickets = data.getTickets(users);

	public void testAutomatedAsign() {

		for (Ticket ticket : tickets) {
			for (User user : users) {
				if (user.getStatus().equals("assignee")) {
					Assignee testAssignee = (Assignee) user;
					int points = auto.getPoints(testAssignee, ticket.getService(), ticket.getPriority());
					printOutPoints(testAssignee,points, ticket);
				}
			}
			Assignee chosen = auto.asignTicket(ticket.getPriority(), ticket.getService(), users);
			printOutChosen(chosen);
		}
	}

	private void printOutPoints(Assignee ass, int points, Ticket ticket) {
		int malus = 0;
		
		System.out.print("Assignee: " + ass.getNameUser() + "reached >" + points + "points<,");
		if (ticket.getService().getRequiredTrait().equals(ass.getMajorTrait())) {
			System.out.print(" his major trait matched, ");
		}
		else if (ticket.getService().getRequiredTrait().equals(ass.getMinorTrait())) {
			System.out.print(" his minor trait matched, ");
		}
		System.out.print("he had: >" + ass.getOpenTickets().size() + " open Tickets< ");
		if (ticket.getPriority().equals("high")) {
			malus = ass.getOpenTickets().size()*3;
		}
		
		else if (ticket.getPriority().equals("low")) {
			malus = ass.getOpenTickets().size()*2;
		}
		System.out.println("so he had a malus of >-"+ malus + " points< ");

	}

	private void printOutChosen(Assignee ass) {
		System.out.println(ass.getNameUser() + " was chosen, lucky bastard");
	}



}
