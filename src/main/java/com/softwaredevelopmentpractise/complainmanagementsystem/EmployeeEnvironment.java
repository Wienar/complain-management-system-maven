/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Luca
 */
public class EmployeeEnvironment extends CommandsAbstract {

    private String greetings = "Hello, \n you are in the Employee Screen "
            + "\n you can work on tickets, and get the status of existing tickets  ";
    private String help = "\n list of all available commands: "
            + "\n getTicket: initiates the ticket process"
            + "\n showTicket: asks you which tickets you want to look at"
            + "\n logout: logs you out and you get back to the login screen"
            + "\n responseTicket(id as int): answers the Ticket with its id"
            + "\n setStatusTicket: changes status of ticket";
    private String error = "";
    private Assignee currentAssignee = null;
    public ArrayList<Ticket> tickets;

    public void start(Assignee assignee) {
        currentAssignee = assignee;
        this.greetings(greetings);
        standby();
    }

    private void standby() {
        String input = this.waitForInput();
        switch (input) {
            case "help":
                this.helpMessage(greetings, help);
                break;
            case "showTicket":
                showTicket();
                break;
            case "responseTicket":
                responseTicket();
                break;
            case "logout":
                break;
            case "setStatusTicket":
                setStatusTicket();
                break;
                
            /*case "getTicket":
                getTicket();
                break;*/
            default:
                this.inputNotSupported();
        }
        if (!input.equals("logout")) {
            standby();
        }
    }

    private void showTicket() {
    System.out.println("This is your current ticket:");    
    ArrayList<Ticket> openTickets = currentAssignee.getOpenTickets();
    Ticket ticket = openTickets.get(0);
        System.out.println("Date:" + ticket.getDateCreated() + "\n" + "Message: " + ticket.getBody() + " \nStatus:" + ticket.getStatus() + " \nCustomer User:" + ticket.getCustomer().getNameUser() + "\nCustomer Name: " + ticket.getCustomer().getNameFirst() + " " + ticket.getCustomer().getNameSecond() + "\nResponse:" + ticket.getResponseAssignee() + "\nFinishedDate:" + ticket.getDateFinished());
    
    }
    
    
    private void responseTicket() {
        System.out.println("Enter your response here:"); //Error message needed for wrong input/not existent input
        ArrayList<Ticket> openTickets = currentAssignee.getOpenTickets();
        Ticket ticket = openTickets.get(0);

        /* String input = scanner.nextLine();
		//String id = input;//Integer.parseInt(input);
                for(Ticket ticket : tickets) {
                    if(ticket.uuid != input){
                    continue;}
                    System.out.println(ticket);
                System.out.println("Please enter your response");*/
        String responseInput = scanner.nextLine();
        // ticket.responseAssignee = ticket.responseAssignee + responseInput;
        ticket.setResponseAssignee(responseInput);

    }
    
    private void setStatusTicket(){
        System.out.println("Change the status of the current ticket to ponding or closed");
        ArrayList<Ticket> openTickets = currentAssignee.getOpenTickets();
        Ticket ticket = openTickets.get(0);
        String responseInput = scanner.nextLine();
        ticket.setStatus(responseInput);
        if(responseInput.equals("closed")){
            Date currentDate = new Timestamp(System.currentTimeMillis());
 
            ticket.setDateFinished(currentDate);
        }
        
    }
}



/*private void showTickets() {
		for (Ticket ticket : tickets) {
			System.out.println(ticket);
	}*/
