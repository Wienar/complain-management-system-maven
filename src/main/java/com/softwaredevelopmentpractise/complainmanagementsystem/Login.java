/*
* This Class is for the Login. Users will be able
* to login as customers or as employee. For the scope of
* the project, this will also operate as a tool to "show"
* different parts of our project
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Login extends CommandsAbstract {

	// attributes
	private final String greetings = "Hello, \n you are in the Login Screen \n please begin the login process with the command \"login\" ";
	private final String help = "\n list of all available commands: \n login: starts the login process \n show : list of all users \n showTickets : list of all tickets \n showdashboard : show the dashboard \n testauto: tests the auto assignment of the tickets";
	private final String askUsername = "please enter your username";
	private final String askPassword = "please enter your password";
	private final String errorLoginFailed = "Username or Password are incorrect, please try again";
        
                // display listtickets 
        
     // private final String listtickets = "Below is the listtickets";
      
        
        //

	ArrayList<User> users;
	ArrayList<Ticket> tickets;
        ArrayList<Listtickets> listtickets;
	public void start() {
		initializeData();
		this.greetings(greetings);
		standby();
	}

	private void standby() {
		String input = this.waitForInput();
		switch (input) {
			case "help":
				this.helpMessage(greetings, help);
				break;
			case "login":
				login();
				break;
			case "show":
				showUsers();
				break;
			case "showTickets":
                              showTickets();
				break;
			case "testAuto":
				testAuto();
				break;
                        case "showdashboard":
                               showdashboard();
                               break;
			default:
				this.inputNotSupported();
		}
		standby();
	}

	private void login() {
		String username = this.messageWithResponse(askUsername);
		String password = this.messageWithResponse(askPassword);
		User user = checkLogin(username, password);
		if (user == null) {
			this.message(errorLoginFailed);
		} else {
			changeTo(user);

		}

	}


	private User checkLogin(String username, String password) {
		for (User user : users) {
			if (user.getNameUser().equals(username)) {
				if (user.getPassword().equals(password)) {
					return user;
				}
			}
		}
		return null;
	}
        
	private void changeTo(User user) {
		switch (user.getStatus()) {
			case "customer":
				UserEnvironment userUI = new UserEnvironment();
				userUI.start((Customer) user);
				break;
			case "assignee":
				EmployeeEnvironment employeeUI = new EmployeeEnvironment();
                                employeeUI.start((Assignee)user);
				break;
			default : this.message("problem LOGIN problem");
		}
		this.greetings(greetings);

	}

	private void initializeData() {
		DataCommands data = new DataCommands();
		users = data.getUsers();
		tickets = data.getTickets(users);
              listtickets = data.getListtickets (users,tickets);
	}

	private void showUsers() {
		for (User user : users) {
			System.out.println(user);
		}
	}

	private void showTickets() {
		for (Ticket ticket : tickets) {
			System.out.println(ticket);
		}
	}
        
      

	private void testAuto() {
		TestCases test = new TestCases();
		test.testAutomatedAsign();
	}

        private void showdashboard(){
            System.out.println("Dashboard for Assignee, Teamlead and User");
            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.printf("| %-10s| %-10s| %-10s| %-15s| %-15s| %-25s| %-25s| %-25s| %-30s|\n", "priotity" , " service ", " status ", " assignee ", "customer ", "dateCreated " ,"dateFinished ", " CustomerEmailID","AssigneeEmailID");
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

            
            listtickets.get(0).choose_sort(listtickets);
            for(Listtickets listticket : listtickets){
                System.out.println(listticket);
            }
          

        }
        
}
