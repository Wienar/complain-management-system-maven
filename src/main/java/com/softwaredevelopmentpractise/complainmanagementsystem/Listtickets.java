/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

/**
 *
 * @author Rucha
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 import java.util.Date;

 /**
 *
 * @author Rucha
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import javax.swing.JTable;
import org.apache.commons.beanutils.BeanComparator;

public class Listtickets extends CommandsAbstract {

    private String uuid;
    String priority;
    private String body;
    private String service;
    String status;
    String assignee;
    private String customer;
    private Date dateCreated;
    private Date dateFinished;
    private String nameUser;
    private String assigneeEmailID;
    private String customerEmailID;

    public Listtickets(String uuid, String priority, String body, String service, String status, String assignee, String customer, Date dateCreated, Date dateFinished, String assigneeEmailID, String customerEmailID) {
  
        this.uuid = uuid;
        this.body = body;
        this.service = service;
        this.priority = priority;
        this.status = status;
        this.assignee = assignee;
        this.customer = customer;
        this.dateCreated = dateCreated;
        this.dateFinished = dateFinished;
        this.assigneeEmailID = assigneeEmailID;
        this.customerEmailID = customerEmailID;

    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getAssigneeEmailID() {
        return assigneeEmailID;
    }

    public void setAssigneeEmailID(String assigneeEmailID) {
        this.assigneeEmailID = assigneeEmailID;
    }

    public String getCustomerEmailID() {
        return customerEmailID;
    }

    public void setCustomerEmailID(String customerEmailID) {
        this.customerEmailID = customerEmailID;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }
    



    
    
    
    public void choose_sort(ArrayList<Listtickets> listTickets) {
        
        System.out.println("Please enter sorting method");
		String sort = this.waitForInput();

             switch (sort) {
		case "status":
                
          
           
                            {
          BeanComparator fieldComparator = new BeanComparator(
                "status");
              Collections.sort(listTickets, fieldComparator);  
                            }
                            break;
                            
                   case "priority":         
                   { BeanComparator fieldComparator1 = new BeanComparator(
                "priority");
    
                 Collections.sort(listTickets, fieldComparator1);  
   
   
                              }
   
                      break;
                      
                   case "assignee":
                       
                        { BeanComparator fieldComparator1 = new BeanComparator(
                "assignee");
    
                 Collections.sort(listTickets, fieldComparator1);  
   
   
                              }
                         break;
                   default:this.inputNotSupported();
		}
		
                     
    }
    @Override
    public String toString() {
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        return  "|" +  priority + "       |"  + service + "    |" +   status   + "      |" +  assignee   + "     |" +   customer + "      |" + dateCreated + "       |" + dateFinished + "       |" +   customerEmailID   + "         |"+ assigneeEmailID + "        |";
        
    }
}
