/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaredevelopmentpractise.complainmanagementsystem;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Assignee extends User {

	private String majorTrait;
	private String minorTrait;
	private ArrayList<Ticket> openTickets;

	public Assignee(String majorTrait, String minorTrait, String nameUser, String nameFirst, String nameSecond, String address, String password, String status, String uuid, String EmailID) {
		super(nameUser, nameFirst, nameSecond, address, password, status, uuid,EmailID);
		this.majorTrait = majorTrait;
		this.minorTrait = minorTrait;
		this.openTickets = new ArrayList<Ticket>();
	}

	public String getMajorTrait() {
		return majorTrait;
	}

	public void setMajorTrait(String majorTrait) {
		this.majorTrait = majorTrait;
	}

	public String getMinorTrait() {
		return minorTrait;
	}

	public void setMinorTrait(String minorTrait) {
		this.minorTrait = minorTrait;
	}

	
	private String printTicketList() {
		String tickets ="";
	 	for (Ticket ticket : openTickets) {
			tickets = tickets + ticket;
		}
		return tickets;
	}

	public ArrayList<Ticket> getOpenTickets() {
		return openTickets;
	}

	public void setOpenTickets(ArrayList<Ticket> openTickets) {
		this.openTickets = openTickets;
	}

	

	@Override
	public String toString() {
		return "Assignee{" + "majorTrait=" + majorTrait + ", minorTrait=" + minorTrait + "nameUser=" + this.getNameUser() + ", nameFirst=" + this.getNameFirst() + ", nameSecond=" + this.getNameSecond() + ", address=" + this.getAddress() + ", password=" + this.getPassword() + ", status=" + this.getStatus() + ", uuid=" + this.getUuid() + "tickets :" +'}';

	}

}
